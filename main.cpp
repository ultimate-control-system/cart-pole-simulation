#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
#include "TorqueControl/TorqueControlPubSubTypes.h"
#include "CartPoleFeedback/CartPoleFeedbackPubSubTypes.h"
#include <cmath>
#include <csignal>

volatile sig_atomic_t Stop;
using namespace eprosima::fastdds::dds;

double X{};
double XDot{};
double XDdot{};
double Theta{};
double ThetaDot{};
double ThetaDdot{};
double MassCart = 10;// kg
double MassPole = 10;// kg
double DampingCart = 0.1;// Ns/m
double DampingPole = 0.5;// Ns/m  ¤ 1.17
double LengthPole = 0.6;// m
double InertiaPole = 0.05;// m^2

double G = 9.81; // m/s^2
double Dt = 0.001; // 0.00001

void
Odes(double force, double &x, double &xDot, double &xDotDot, double &theta, double &thetaDot, double &thetaDotDot) {
    xDotDot = (force - DampingCart * xDot + MassPole * LengthPole * thetaDotDot * cos(theta) -
               MassPole * LengthPole * pow(thetaDot, 2) * sin(theta)) / (MassCart + MassPole);
    thetaDotDot = (-DampingPole * thetaDot + MassPole * LengthPole * G * sin(theta) +
                   MassPole * LengthPole * xDotDot * cos(theta)) / (InertiaPole + MassPole * pow(LengthPole, 2));
    xDot += xDotDot * Dt;
    thetaDot += thetaDotDot * Dt;
    x += xDot * Dt;
    theta += thetaDot * Dt;
}

double Torque;
DomainParticipant *PParticipant;
DdsPublisher<CartPoleFeedbackPubSubType> *FeedbackPublisher;
DdsSubscriber<TorqueControlPubSubType> *TorqueSubscriber;

int main() {
    signal(SIGTERM, [](int signum) { Stop = 1; });
    Theta = 1;
    X = 0;
    DomainParticipantQos participantQos;
    participantQos.name("Cart pole simulator");
    PParticipant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (PParticipant == nullptr) {
        return 0;
    }
    // Create publishers
    FeedbackPublisher = new DdsPublisher<CartPoleFeedbackPubSubType>(PParticipant, "CartPoleFeedback");
    FeedbackPublisher->bindPublicationMatchedCallback([](const PublicationMatchedStatus &info) {

        std::cout << "Current subscribers count = " << FeedbackPublisher->subscriberCount() << std::endl;
    });

    TorqueSubscriber = new DdsSubscriber<TorqueControlPubSubType>(PParticipant, "TorqueControl");
    TorqueSubscriber->bindDataAvailableCallback([=](const TorqueControl &msg, const SampleInfo &info) {
        Torque = msg.torque_nm();
    });

    CartPoleFeedback feedback;

    while (!Stop) {
        Odes(Torque, X, XDot, XDdot, Theta, ThetaDot, ThetaDdot);
        feedback.cart_x() = X;
        feedback.cart_x_dot() = XDot;
        feedback.cart_x_dotdot() = XDdot;

        feedback.pole_theta() = Theta;
        feedback.pole_theta_dot() = ThetaDot;
        feedback.pole_theta_dotdot() = ThetaDdot;
        FeedbackPublisher->publish(feedback);

        std::this_thread::sleep_for(std::chrono::microseconds(long(Dt * 1000000)));
    }

    return 0;
}
